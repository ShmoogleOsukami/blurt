# Building Blurt

Blurt is designed to be compiled on GitLab. By using `ccache` in our build process, we have been able to reduce compile times for Blurt from as high as 3 hours when using shared GitLab Runners to as little as 3 minutes when using our own runners with the following spec:

- AMD Ryzen 9 3900 12 Cores "Matisse" (Zen2)
- 128 GB DDR4 ECC RAM
- 2 x 1.92 TB NVMe SSD Datacenter Edition (Software-RAID 1)
- 1 Gbit/s bandwidth

If you're working on Blurt and you'd like to use our infrastructure to compile your builds, please just e-mail info@blurt.foundation or create an issue, and we will get you set up on a branch.

We're shooting for 1-2 minute compile times in the end, so that we can achieve the highest level of developer productivity. We are mainly taking advice from these articles:

[Ccache for Gitlab CI](https://gould.cx/ted/blog/2017/06/10/ccache-for-Gitlab-CI/)
[Faster C Builds](https://www.bitsnbites.eu/faster-c-builds/)
[12 Steps to Better Code](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/)

If you would like to compile Blurt locally, you can do that as well! Our .gitlab-ci.yml file provides a clear description of how to set up a Blurt devleopment environment on Debian 10.

If you fork Blurt into another GitLab repository, the build system is complete and will be reasonably fast after the first build, even on shared GitLab runners. Note that you may need to extend the timeout to 3 hours for the first build if using shared GitLab runners.

**Please upstream changes!**

---

## Compile-Time Options (cmake)

### CMAKE_BUILD_TYPE=[Release/Debug]

Specifies whether to build with or without optimization and without or with
the symbol table for debugging. Unless you are specifically debugging or
running tests, it is recommended to build as release.

### LOW_MEMORY_NODE=[OFF/ON]

Builds blurtd to be a consensus-only low memory node. Data and fields not
needed for consensus are not stored in the object database. This option is
recommended for witnesses and seed-nodes.

### CLEAR_VOTES=[ON/OFF]

Clears old votes from memory that are no longer required for consensus.

### BUILD_BLURT_TESTNET=[OFF/ON]

Builds blurt for use in a private testnet. Also required for building unit tests.

### SKIP_BY_TX_ID=[OFF/ON]

By default this is off. Enabling will prevent the account history plugin querying transactions
by id, but saving around 65% of CPU time when reindexing. Enabling this option is a
huge gain if you do not need this functionality.

---

## Building under Docker

We ship a [Dockerfile](https://gitlab.com/blurt/docker). This builds both common node type binaries.

## Building on Linux (Ubuntu Desktop/Server 20.04)

If you are building this on a VPS you will likely need a swapfile. 12GB is recommended.

```
sudo fallocate -l 12G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
```

### Install build dependencies

Make sure you have lots of free space, Compiling can take up to 20GB in some instances.

Your first compile will take quite a long time (depending on hardware) as hunter will fetch and compile all required libs. e.g boost, etc.

```
    # Update your OS and install git, cmake, build-essential, python3-jinja2, automake, libtool, doxygen

    sudo apt update
    sudo apt upgrade -y
    sudo apt install -y git cmake build-essential python3-jinja2 automake libtool doxygen pkg-config

    # Clone the Blurt Repository

    git clone https://gitlab.com/blurt/blurt
    cd blurt
    mkdir build && cd build

    #Configure cmake ready to compile

    cmake -DBLURT_STATIC_BUILD=ON -DLOW_MEMORY_NODE=OFF -DCLEAR_VOTES=OFF -DBUILD_BLURT_TESTNET=OFF -DSKIP_BY_TX_ID=OFF -DBLURT_LINT_LEVEL=OFF -DENABLE_MIRA=OFF -DSKIP_FEE_VOP=ON -DCMAKE_BUILD_TYPE=Release ..

    # Compile
    # Note: you can use -j1 or whatever number you have equal to the cores your vm has. The higher the -j value the more ram & swap required.

    make -j$(nproc) blurtd cli_wallet

    # Install
    # Note: I'd recommend first doing 'make install' and then redoing the command under SUDO. Depending on your cofiguration MAKE may not have permissions to install blurtd but using sudo make install staight away may cause hunter to rebuild all depends in the root directory wasting time and space.

    sudo make install
```
